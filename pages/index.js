import * as Components from '../components/index';


export default function Home() {
  return (
      <div className="flex flex-col bg-gray-200">
          <Components.About />
        <div className="bg-gray-100 h-20 md:h-40 transform-gpu skew-y-6 rotate-11 md:-translate-y-20 -translate-y-5 translate-y-10 z-0">
        </div>
        
        <Components.Work />
        <div className="bg-gray-100 h-40 transform -skew-y-6 rotate-11 md:translate-y-20 translate-y-10 z-0">
        </div>
          <Components.Contact/>
          <Components.Footer/>
      </div>
  )
}
