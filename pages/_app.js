import Head from 'next/head';
import { Provider } from 'react-redux';
import store from '../redux/store';
import * as Components from '../components/index';
import '../styles/globals.css'

function App({ Component, pageProps }) {
  return (<>
      <Head>
        {/* Global Site Tag (gtag.js) - Google Analytics */}
        <script
            async
            src={`https://www.googletagmanager.com/gtag/js?id=${process.env.GOOGLE_ANALYTICS}`}
          />
          <script
            dangerouslySetInnerHTML={{
              __html: `
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', '${process.env.GOOGLE_ANALYTICS}', {
              page_path: window.location.pathname,
            });
          `,
            }}
          />
        <title>Tiago Pratas' Portfolio</title>
        <meta name="description" content="Page for Tiago Prata's online Portfolio. A fullstack MERN dev living in Madrid, Spain"></meta>
        <link rel="icon" href="/portfolio_logo.png"/>
        <link rel="preconnect" href="https://fonts.googleapis.com"/>
        <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin />
        <link href="https://fonts.googleapis.com/css2?family=Bungee+Shade&display=swap" rel="stylesheet" />
        <script async src="https://www.googletagmanager.com/gtag/js?id=G-87JCN54VY9"></script>
      </Head>
    <Provider store={store}>
      <div className="bg-gray-100 h-full">
        <Components.Header/>
        <div className="flex justify-center">
          <Component {...pageProps} />
        </div>
      </div>
    </Provider>
    </>
  )
}

export default App;
