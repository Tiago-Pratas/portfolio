import infoSlice from './slices/info.slice.js';
import { configureStore } from '@reduxjs/toolkit'

export default configureStore({
    reducer: {

        info: infoSlice.reducer,
    
    }
});