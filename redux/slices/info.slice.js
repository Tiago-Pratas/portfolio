import { createSlice } from '@reduxjs/toolkit';

const INITIAL_STATE = {
    personalInfo: {
        name: 'Tiago',
        surnames: 'Azevedo Pratas',
        job: 'Developer of Stuff',
        about: `Hi! I am Tiago & I am a web developer`,
        about2:[' React', 'Nodejs', 'Nextjs', 'NoSQL', 'Javascript', 'Typescript', 'Death Metal'],
        links: {
            email: 'prattiago@gmail.com',
            linkedin: 'https://www.linkedin.com/in/Tiago-Pratas',
            twitter: 'Twitter/Tiago-Pratas',
            dev: 'Dev.to/TiagoPratas',
            git: 'www.github.com',
        },
    },
    projects: [
        {
            title: 'The comic books ',
            img: 'https://i.ibb.co/N6vQhFF/Screenshot-2021-06-21-at-09-12-54.png',
            live: 'https://the-comic-books.vercel.app',
            repo: 'https://github.com/Tiago-Pratas/the-comic-books',
            status: 'in progress',
            description: 'is a service that allows users to track their comics collection and tarde issues with other users',
            technologies: [' React', ' NodeJS', ' MongoDB', ' Nodemailer', ' Handlebars', ' Passport']
        },
        {
        title: 'Kaban Board ',
        img: 'https://github.com/Tiago-Pratas/Kabanboard/raw/master/src/assets/KabanBoard.png',
        live: 'https://kabanboard-hgqtvhyly-tiago-pratas.vercel.app',
        repo: 'https://github.com/Tiago-Pratas/Kabanboard',
        status: 'finished',
        description: 'is a.. well... Kaban Board made with Angular and Material UI.',
        technologies: [' Angular', ' Material UI', ' Typescript']
        },
        {
            title: 'Streamly ',
            img: 'https://i.ibb.co/7b4vV3d/Screenshot-2021-06-23-at-08-24-21.png',
            live: 'https://streamly.netlify.app/',
            repo: 'https://gitlab.com/streamly-app',
            status: 'in progress',
            description: 'is a service that allows users to track their favourite OTTs and can generate random recommendations based on different criteria',
            technologies: [' React', ' NodeJS', ' MongoDB', ' Nodemailer']
        },
        {
            title: 'Portfolio ',
            img: 'https://i.ibb.co/23GLDWV/Screenshot-2021-06-23-at-15-01-37.png',
            live: 'https://tiago-pratas.vercel.app',
            repo: 'https://gitlab.com/Tiago-Pratas/portfolio',
            status: 'finished',
            description: 'My own portfolio which you are looking at right now.',
            technologies: [' React', ' NodeJS', ' NextJS']
            },
    ],


};

const infoSlice = createSlice({
    name: 'info',
    initialState: INITIAL_STATE,
});

export default infoSlice;