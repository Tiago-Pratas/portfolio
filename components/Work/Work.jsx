import React from 'react';
import Image from 'next/image';
import { useSelector } from 'react-redux'
import { motion } from "framer-motion"

const Work = () => {
    const { projects } = useSelector(state => state.info);

    return <section className="text-gray-600 body-font bg-gray-200 " id="work">
    <div className="container px-5 py-2 mx-auto">
        <div className="flex flex-wrap w-full mb-10">
            <div className="lg:w-1/2 w-full mb-6 lg:mb-0">
                <p className="sm:text-3xl text-2xl font-metal-mania font-medium title-font mb-2 bg-clip-text bg-gradient-to-tl from-blue-300 via-black to-red-500">Things I made and contributed to</p>
                <div className="h-1 w-70 rounded bg-gradient-to-r from-blue-300 via-black to-red-500"></div>
            </div>
        </div>
    </div>
    <div className="p-5">
        {
            projects.map((project, index) => (<motion.div className="p-5" initial={true}
            animate="show" animate={{ x: 10 }}
            transition={{ ease: "easeOut", duration: 0.5, delayChildren: 1 }}>
            <div className="relative rounded-lg block md:flex items-center bg-gray-100 shadow-2xl" key={index}>
              <div className="relative w-full md:w-2/5 h-full 
                    overflow-hidden rounded-t-lg 
                    md:rounded-t-none md:rounded-l-lg" >
                <img className=" inset-0 w-full h-full object-cover 
                    object-center opacity-670" 
                    src={project.img} 
                    alt={project.title}
                    width={250} height={250}/>
                <div className=" inset-0 w-full h-full 
                    bg-red-400 opacity-75"></div>
              </div>
              <div className=" w-full md:w-3/5 
                    h-full flex items-center bg-gray-100 rounded-lg">
                <div className=" p-12 md:pr-24 md:pl-16 md:py-12">
                  <p className=" text-gray-600">
                    <span className="text-gray-900 font-bold">
                      {project.title}
                    </span>
                    {project.description}
                  </p>
                  <h3 className="font-bold">Stack : </h3> 
                    {
                      project.technologies.map((technology, index) => <span key={index}> - {technology}</span>)
                    }
                  
                  <a className=" flex items-baseline mt-3 text-blue-400 hover:text-red-600 focus:text-red-400" href={project.live}>
                    <span>Live</span>
                    <span className="text-xs ml-1">&#x279c;</span>
                  </a>
                  <a className=" flex items-baseline mt-3 text-blue-400 hover:text-red-600 focus:text-red-400" href={project.repo}>
                    <span>Repo</span>
                    <span className="text-xs ml-1">&#x279c;</span>
                  </a>
                </div>
                <svg className="hidden md:block absolute inset-y-0 h-full w-24 fill-current text-gray-100 -ml-12" viewBox="0 0 100 100" preserveAspectRatio="none">
                  <polygon points="50,0 100,0 50,100 0,100" />
                </svg>
              </div>
            </div>
      </motion.div>
            ))
        }

    </div>
    
    </section>;
};

export default Work;