import React from 'react';
import Image from 'next/image';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faGithub,faDev, faLinkedin} from '@fortawesome/free-brands-svg-icons';

const Footer = () => {
    return <footer className="text-gray-600 body-font bg-gray-300 z-40">
        <div className="container px-5 py-8 mx-auto flex items-center sm:flex-row flex-col">
            <a className="flex title-font font-medium items-center md:justify-start justify-center text-gray-900">
                <Image src="/portfolio_logo.png" width={60} height={60} className="w-10 h-10p-2 bg-gray-300 rounded-full"/>
            </a>
            <p className="text-sm text-gray-500 sm:ml-4 sm:pl-4 sm:border-l-2 sm:border-gray-200 sm:py-2 sm:mt-0 mt-4">
                © 2021 Tiago Pratas  —            
            </p>
            <span className="inline-flex sm:ml-auto sm:mt-0 mt-4 justify-center sm:justify-start">
                <a href='https://www.linkedin.com/in/tiago-pratas'>
                    <FontAwesomeIcon icon={faLinkedin} className="h-8 w-8"/>
                </a>
                <a href='https://github.com/Tiago-Pratas'>
                    <FontAwesomeIcon icon={faGithub} className="h-8 w-8"/>
                </a>
                <a href='https://dev.to/tiagopratas'>
                    <FontAwesomeIcon icon={faDev} className="h-8 w-8"/>
                </a>
            </span>
        </div>
  </footer>
};

export default Footer;