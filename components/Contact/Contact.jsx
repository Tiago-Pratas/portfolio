import { useState } from 'react';

const Contact = () => {

    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [message, setMessage] = useState('');
    const [submitted, setSubmitted] = useState(false);

    const handleSubmit = (e) => { 
        e.preventDefault()

        console.log('Sending');

        let data = {
            name,
            email,
            message
            };

        fetch('/api/contact', {
            method: 'POST',
            headers: {
                'Accept': 'application/json, text/plain, */*',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
            })
            .then((res) => {
            console.log('Response received')
            if (res.status === 200) {
                console.log('Response succeeded!')
                setSubmitted(true)
                setName('')
                setEmail('')
                setMessage('')
                }
            }
        )
    }
    return(
    <div className="bg-gray-100 text-gray-600 bg-fixed z-30 " id="contact">
        <div className="container px-5 py-2 mx-auto">
        <div className="flex flex-wrap w-full mb-10">
            <div className="lg:w-1/2 w-full mb-6 lg:mb-0">
                <p className="sm:text-3xl text-2xl font-metal-mania font-medium title-font mb-2 bg-clip-text bg-gradient-to-tl from-blue-300 via-black to-red-500">Send me an email</p>
                <div className="h-1 w-70 rounded bg-gradient-to-r from-blue-300 via-black to-red-500"></div>
            </div>
        </div>
    </div>
        <div className="max-w-2xl bg-transparent px-5 m-auto w-full mt-10">


                </div>
        <div className="grid grid-cols-2 gap-4 max-w-xl m-auto p-3">

            <div className="col-span-2 lg:col-span-1">
                <input type="text" className="opacity-80 border-solid border-gray-400 rounded border-2 p-3 md:text-xl w-full"
                onChange={(name)=>{setName(name.target.value)}} 
                placeholder="Name" name="name"/>
            </div>

            <div className="col-span-2 lg:col-span-1">
                <input type="text" className="opacity-80 border-solid border-gray-400 rounded border-2 p-3 md:text-xl w-full"
                onChange={(email)=>{setEmail(email.target.value)}} 
                placeholder="Email Address" name="email"/>
            </div>

            <div className="col-span-2">
                <textarea cols="30" rows="8" className="opacity-80 ring-green-800 border-solid rounded border-gray-400 border-2 p-3 md:text-xl w-full" 
                placeholder="Message"
                onChange={(message)=>{setMessage(message.target.value)}} 
                name="message"></textarea>
            </div>

            <div className="col-span-2 text-right">
                <button className="py-3 opacity-90 px-6 ring-0 bg-gradient-to-tl from-white via-black to-red-600 text-gray-400 font-bold w-full sm:w-32 rounded-lg shadow-lg"
                    onClick={(e)=>{handleSubmit(e)}}>
                        Send
                </button>
            </div>
            </div>
    </div>

    )
};

export default Contact;