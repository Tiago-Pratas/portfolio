import React from 'react';
import Image from 'next/image'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faGithub,faDev, faLinkedin} from '@fortawesome/free-brands-svg-icons';

const Header = () => {
    return <header className="text-gray-600 body-font">
    <div className="flex flex-wrap p-5 flex-col md:flex-row items-center bg-gray-400 rounded">
      <a className="flex title-font font-medium items-center text-gray-900 mb-4 md:mb-0 border-r-2 border-gray-300 px-3">
        <Image src="/portfolio_logo.png" width={60} height={60} className="w-10 h-10p-2 bg-gray-300 rounded-full"/>
      </a>
      <span className="inline-flex sm:ml-auto sm:mt-0 mt-4 justify-center sm:justify-start text-gray-300">
          <a href='https://www.linkedin.com/in/tiago-pratas'>
            <FontAwesomeIcon icon={faLinkedin} className="h-10 w-10 p-1 "/>
          </a>
          <a href='https://github.com/Tiago-Pratas'>
            <FontAwesomeIcon icon={faGithub} className="h-10 w-10 p-1"/>
          </a>
          <a href='https://dev.to/tiagopratas'>
            <FontAwesomeIcon icon={faDev} className="h-10 w-10 p-1"/>
          </a>
        </span>
      <nav className="md:ml-auto md:mr-auto flex flex-wrap items-center text-base justify-center">
        <a className="mr-5 text-gray-200 text-2xl hover:text-blue-300 cursor-pointer" href='#about'>About me</a>
        <a className="mr-5 text-gray-200 text-2xl hover:text-blue-300 cursor-pointer" href='#about'>Work</a>
        <a className="mr-5  text-gray-200 text-2xl hover:text-blue-300 cursor-pointer" href='#contact'>Contact</a>
      </nav>
    </div>
    
  </header>
};

export default Header;