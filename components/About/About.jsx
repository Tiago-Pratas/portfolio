import React, { useRef, useEffect } from 'react';
import Image from 'next/image';
import { useSelector } from 'react-redux'
import Typed from 'typed.js'


const About = () => {
    const { personalInfo } = useSelector(state => state.info);
    const el = useRef(null);
    const typed = useRef(null);

    useEffect(() => {
        const options = {
            strings: personalInfo.about2,
            typeSpeed: 60,
            backSpeed: 60,
            loop: true,
        };
        
        // elRef refers to the <span> rendered below
        typed.current = new Typed(el.current, options);
        
        return () => {
            // Make sure to destroy Typed instance during cleanup
            // to prevent memory leaks
            typed.current.destroy();
        }
    }, [])

    return(
<div className="bg-gray-100 z-40 " id='about'>
    <div className="grid col-span-1 md:flex items-center mt-10 justify-center z-50 bg-gray-100">
        <div className="mr-14 px-4">
            <Image className="opacity-80" src="/avataaars.png" width={200} height={200} alt="avatar"/>
        
        </div>
        
        <div className="mr-4 ">
            <Image className="w-50" src='/portfolio_animation.gif' width={250} height={250} alt="logo"/>
        </div>
        <div className="md:border-l-2 pl-4 p-2 col-span-2 md:w-1/2 mt-10 md:mt-0">
            <div>
                <h1 className="text-4xl text-left md:text-6xl font-metal-mania text-transparent bg-clip-text bg-gradient-to-br from-blue-300 via-black to-red-500">
                    {personalInfo.about}
                </h1>
            </div>
            <div className="type-wrap text-3xl text-transparent bg-clip-text bg-gradient-to-l from-blue-300 via-black to-red-500">
                    <span className="p-3">I &#9829; </span> <span className="font-metal-mania" ref={el} />
            </div>
        </div>
    </div>

    
    

            </div>
    )
}

export default About;